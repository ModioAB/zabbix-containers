IMAGE_REGISTRY_NAMESPACE ?= registry.gitlab.com/modioab
SUBDIR ?= $(patsubst %/,%,$(shell git rev-parse --show-prefix))
IMAGE_REPO ?= $(IMAGE_REGISTRY_NAMESPACE)/zabbix-containers/$(SUBDIR)
