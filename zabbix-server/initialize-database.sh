#!/bin/bash -e

# The default owner role is named like the database
: ${DB_OWNER:=${PGDATABASE}}


preflight() {
     test -v ZABBIX_API_USER  || (echo "ZABBIX_API_USER unset"; exit 1)
     test -v ZABBIX_API_PASSWORD  || (echo "ZABBIX_API_PASSWORD unset"; exit 1)
}

oneshot() {
    ( echo 'SET ROLE :"dbowner";'
      zcat "/usr/share/doc/zabbix-server-pgsql/create.sql.gz"
    ) | /usr/bin/psql --set ON_ERROR_STOP=on --set dbowner="${DB_OWNER}"
    ( echo 'SET ROLE :"dbowner";'
      cat "/usr/share/zabbix-pg/partitions.sql"
    ) | /usr/bin/psql --set ON_ERROR_STOP=on --set dbowner="${DB_OWNER}"
}

fixup() {
    ( echo 'SET ROLE :"dbowner";'
      cat "/usr/share/zabbix-pg/fixup.sql"
    ) | /usr/bin/psql --set ON_ERROR_STOP=on \
            --set dbowner="${DB_OWNER}" \
            --set submituser="${ZABBIX_API_USER}" \
            --set submitpass="${ZABBIX_API_PASSWORD}"
}

usage() {
    echo "Accepting commands:"
    echo "    oneshot"
    echo "    fixup"
}

case "$1" in
    oneshot )
        oneshot
    ;;
    fixup )
        preflight
        fixup
    ;;
    * )
        usage
        exit 1
esac
