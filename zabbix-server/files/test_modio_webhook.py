"""Test the parsing of the action message format."""

import pytest

import modio_webhook


PROBLEM_MESSAGE = """\
[webhook-event-values]
SEPARATOR=--.Yup38>&@AfooQ55*`nHefFI8c"GL6pTSe<cjQ70--
EVENT.ID=1177218
--.Yup38>&@AfooQ55*`nHefFI8c"GL6pTSe<cjQ70--
EVENT.RECOVERY.ID={EVENT.RECOVERY.ID}
--.Yup38>&@AfooQ55*`nHefFI8c"GL6pTSe<cjQ70--
HOST.HOST=40bd32e308dc
--.Yup38>&@AfooQ55*`nHefFI8c"GL6pTSe<cjQ70--
ITEM.KEY=mytemp.status
--.Yup38>&@AfooQ55*`nHefFI8c"GL6pTSe<cjQ70--
TRIGGER.STATUS=PROBLEM
--.Yup38>&@AfooQ55*`nHefFI8c"GL6pTSe<cjQ70--
HOST.DESCRIPTION=Nilis Skrivbord
--.Yup38>&@AfooQ55*`nHefFI8c"GL6pTSe<cjQ70--
TRIGGER.ID=14389
--.Yup38>&@AfooQ55*`nHefFI8c"GL6pTSe<cjQ70--
TRIGGER.SEVERITY=Information
--.Yup38>&@AfooQ55*`nHefFI8c"GL6pTSe<cjQ70--
TRIGGER.NSEVERITY=1
--.Yup38>&@AfooQ55*`nHefFI8c"GL6pTSe<cjQ70--
EVENT.DATE=2020.04.24
--.Yup38>&@AfooQ55*`nHefFI8c"GL6pTSe<cjQ70--
EVENT.TIME=15:01:22
--.Yup38>&@AfooQ55*`nHefFI8c"GL6pTSe<cjQ70--
EVENT.RECOVERY.DATE={EVENT.RECOVERY.DATE}
--.Yup38>&@AfooQ55*`nHefFI8c"GL6pTSe<cjQ70--
EVENT.RECOVERY.TIME={EVENT.RECOVERY.TIME}
--.Yup38>&@AfooQ55*`nHefFI8c"GL6pTSe<cjQ70--
TRIGGER.NAME=Test
--.Yup38>&@AfooQ55*`nHefFI8c"GL6pTSe<cjQ70--"""


def test_parsing_problem_messages_should_work():
    payload = modio_webhook.json_payload_from_alert_message(PROBLEM_MESSAGE)

    assert payload == {
        "event": {
            "clock": 1587740482,
            "description": "Test",
            "eventid": "1177218",
            "host": "40bd32e308dc",
            "key": "mytemp.status",
            "severity": ("1", "Information"),
            "value": "problem",
        },
        "trigger": {
            "description": "Test",
            "id": "14389",
            "lastchange": 1587740482,
            "severity": ("1", "Information"),
            "value": "problem",
        },
    }


RECOVERY_MESSAGE = """\
[webhook-event-values]
SEPARATOR=--.Yup38>&@AfooQ55*`nHefFI8c"GL6pTSe<cjQ70--
EVENT.ID=1177218
--.Yup38>&@AfooQ55*`nHefFI8c"GL6pTSe<cjQ70--
EVENT.RECOVERY.ID=1177223
--.Yup38>&@AfooQ55*`nHefFI8c"GL6pTSe<cjQ70--
HOST.HOST=40bd32e308dc
--.Yup38>&@AfooQ55*`nHefFI8c"GL6pTSe<cjQ70--
ITEM.KEY=mytemp.status
--.Yup38>&@AfooQ55*`nHefFI8c"GL6pTSe<cjQ70--
TRIGGER.STATUS=OK
--.Yup38>&@AfooQ55*`nHefFI8c"GL6pTSe<cjQ70--
HOST.DESCRIPTION=Nilis Skrivbord
--.Yup38>&@AfooQ55*`nHefFI8c"GL6pTSe<cjQ70--
TRIGGER.ID=14389
--.Yup38>&@AfooQ55*`nHefFI8c"GL6pTSe<cjQ70--
TRIGGER.SEVERITY=Information
--.Yup38>&@AfooQ55*`nHefFI8c"GL6pTSe<cjQ70--
TRIGGER.NSEVERITY=1
--.Yup38>&@AfooQ55*`nHefFI8c"GL6pTSe<cjQ70--
EVENT.DATE=2020.04.24
--.Yup38>&@AfooQ55*`nHefFI8c"GL6pTSe<cjQ70--
EVENT.TIME=15:01:22
--.Yup38>&@AfooQ55*`nHefFI8c"GL6pTSe<cjQ70--
EVENT.RECOVERY.DATE=2020.04.24
--.Yup38>&@AfooQ55*`nHefFI8c"GL6pTSe<cjQ70--
EVENT.RECOVERY.TIME=15:04:00
--.Yup38>&@AfooQ55*`nHefFI8c"GL6pTSe<cjQ70--
TRIGGER.NAME=Test
--.Yup38>&@AfooQ55*`nHefFI8c"GL6pTSe<cjQ70--"""


def test_parsing_recovery_messages_should_work():
    payload = modio_webhook.json_payload_from_alert_message(RECOVERY_MESSAGE)

    assert payload == {
        "event": {
            "clock": 1587740640,
            "description": "Test",
            "eventid": "1177223",
            "host": "40bd32e308dc",
            "key": "mytemp.status",
            "severity": ("1", "Information"),
            "value": "OK",
        },
        "trigger": {
            "description": "Test",
            "id": "14389",
            "lastchange": 1587740640,
            "severity": ("1", "Information"),
            "value": "OK",
        },
    }


PROBLEM_MESSAGE_2 = """\
[webhook-event-values]
SEPARATOR=RANDOM_STRING_01
EVENT.ID=1177218
RANDOM_STRING_01
EVENT.RECOVERY.ID={EVENT.RECOVERY.ID}
RANDOM_STRING_01
HOST.HOST=40bd32e308dc
RANDOM_STRING_01
ITEM.KEY=mytemp.status
RANDOM_STRING_01
TRIGGER.STATUS=PROBLEM
RANDOM_STRING_01
HOST.DESCRIPTION=Nilis Skrivbord
RANDOM_STRING_01
TRIGGER.ID=14389
RANDOM_STRING_01
TRIGGER.SEVERITY=Information
RANDOM_STRING_01
TRIGGER.NSEVERITY=1
RANDOM_STRING_01
EVENT.DATE=2020.04.24
RANDOM_STRING_01
EVENT.TIME=15:01:22
RANDOM_STRING_01
EVENT.RECOVERY.DATE={EVENT.RECOVERY.DATE}
RANDOM_STRING_01
EVENT.RECOVERY.TIME={EVENT.RECOVERY.TIME}
RANDOM_STRING_01
TRIGGER.NAME=Test 2
RANDOM_STRING_01"""


def test_different_separator_strings_should_work():
    payload = modio_webhook.json_payload_from_alert_message(PROBLEM_MESSAGE_2)

    assert payload == {
        "event": {
            "clock": 1587740482,
            "description": "Test 2",
            "eventid": "1177218",
            "host": "40bd32e308dc",
            "key": "mytemp.status",
            "severity": ("1", "Information"),
            "value": "problem",
        },
        "trigger": {
            "description": "Test 2",
            "id": "14389",
            "lastchange": 1587740482,
            "severity": ("1", "Information"),
            "value": "problem",
        },
    }


MISSING_SEPARATOR_MESSAGE = """\
[webhook-event-values]
SEPARATOR=--.Yup38>&@AfooQ55*`nHefFI8c"GL6pTSe<cjQ70--
EVENT.ID=1177218
EVENT.RECOVERY.ID={EVENT.RECOVERY.ID}
--.Yup38>&@AfooQ55*`nHefFI8c"GL6pTSe<cjQ70--
HOST.HOST=40bd32e308dc
--.Yup38>&@AfooQ55*`nHefFI8c"GL6pTSe<cjQ70--
ITEM.KEY=mytemp.status
--.Yup38>&@AfooQ55*`nHefFI8c"GL6pTSe<cjQ70--
TRIGGER.STATUS=PROBLEM
--.Yup38>&@AfooQ55*`nHefFI8c"GL6pTSe<cjQ70--
HOST.DESCRIPTION=Nilis Skrivbord
--.Yup38>&@AfooQ55*`nHefFI8c"GL6pTSe<cjQ70--
TRIGGER.ID=14389
--.Yup38>&@AfooQ55*`nHefFI8c"GL6pTSe<cjQ70--
TRIGGER.SEVERITY=Information
--.Yup38>&@AfooQ55*`nHefFI8c"GL6pTSe<cjQ70--
TRIGGER.NSEVERITY=1
--.Yup38>&@AfooQ55*`nHefFI8c"GL6pTSe<cjQ70--
EVENT.DATE=2020.04.24
--.Yup38>&@AfooQ55*`nHefFI8c"GL6pTSe<cjQ70--
EVENT.TIME=15:01:22
--.Yup38>&@AfooQ55*`nHefFI8c"GL6pTSe<cjQ70--
EVENT.RECOVERY.DATE={EVENT.RECOVERY.DATE}
--.Yup38>&@AfooQ55*`nHefFI8c"GL6pTSe<cjQ70--
EVENT.RECOVERY.TIME={EVENT.RECOVERY.TIME}
--.Yup38>&@AfooQ55*`nHefFI8c"GL6pTSe<cjQ70--"""


def test_messages_with_missing_separators_should_be_rejected():
    with pytest.raises(modio_webhook.SeparatorMissing):
        modio_webhook.json_payload_from_alert_message(
            MISSING_SEPARATOR_MESSAGE
        )


MISSING_VALUE_MESSAGE = """\
[webhook-event-values]
SEPARATOR=--.Yup38>&@AfooQ55*`nHefFI8c"GL6pTSe<cjQ70--
EVENT.ID=1177218
--.Yup38>&@AfooQ55*`nHefFI8c"GL6pTSe<cjQ70--
EVENT.RECOVERY.ID={EVENT.RECOVERY.ID}
--.Yup38>&@AfooQ55*`nHefFI8c"GL6pTSe<cjQ70--
HOST.HOST=40bd32e308dc
--.Yup38>&@AfooQ55*`nHefFI8c"GL6pTSe<cjQ70--
ITEM.KEY=mytemp.status
--.Yup38>&@AfooQ55*`nHefFI8c"GL6pTSe<cjQ70--
TRIGGER.STATUS=PROBLEM
--.Yup38>&@AfooQ55*`nHefFI8c"GL6pTSe<cjQ70--
HOST.DESCRIPTION=Nilis Skrivbord
--.Yup38>&@AfooQ55*`nHefFI8c"GL6pTSe<cjQ70--
TRIGGER.ID=14389
--.Yup38>&@AfooQ55*`nHefFI8c"GL6pTSe<cjQ70--
TRIGGER.SEVERITY=Information
--.Yup38>&@AfooQ55*`nHefFI8c"GL6pTSe<cjQ70--
TRIGGER.NSEVERITY=1
--.Yup38>&@AfooQ55*`nHefFI8c"GL6pTSe<cjQ70--
EVENT.DATE=2020.04.24
--.Yup38>&@AfooQ55*`nHefFI8c"GL6pTSe<cjQ70--
EVENT.TIME=15:01:22
--.Yup38>&@AfooQ55*`nHefFI8c"GL6pTSe<cjQ70--
EVENT.RECOVERY.DATE={EVENT.RECOVERY.DATE}
--.Yup38>&@AfooQ55*`nHefFI8c"GL6pTSe<cjQ70--
EVENT.RECOVERY.TIME={EVENT.RECOVERY.TIME}
--.Yup38>&@AfooQ55*`nHefFI8c"GL6pTSe<cjQ70--"""


def test_messages_with_missing_values_should_be_rejected():
    with pytest.raises(modio_webhook.ValueMissing):
        modio_webhook.json_payload_from_alert_message(MISSING_VALUE_MESSAGE)


MISSING_LAST_SEPARATOR_MESSAGE = """\
[webhook-event-values]
SEPARATOR=--.Yup38>&@AfooQ55*`nHefFI8c"GL6pTSe<cjQ70--
EVENT.ID=1177218
--.Yup38>&@AfooQ55*`nHefFI8c"GL6pTSe<cjQ70--
EVENT.RECOVERY.ID={EVENT.RECOVERY.ID}
--.Yup38>&@AfooQ55*`nHefFI8c"GL6pTSe<cjQ70--
HOST.HOST=40bd32e308dc
--.Yup38>&@AfooQ55*`nHefFI8c"GL6pTSe<cjQ70--
ITEM.KEY=mytemp.status
--.Yup38>&@AfooQ55*`nHefFI8c"GL6pTSe<cjQ70--
TRIGGER.STATUS=PROBLEM
--.Yup38>&@AfooQ55*`nHefFI8c"GL6pTSe<cjQ70--
HOST.DESCRIPTION=Nilis Skrivbord
--.Yup38>&@AfooQ55*`nHefFI8c"GL6pTSe<cjQ70--
TRIGGER.ID=14389
--.Yup38>&@AfooQ55*`nHefFI8c"GL6pTSe<cjQ70--
TRIGGER.SEVERITY=Information
--.Yup38>&@AfooQ55*`nHefFI8c"GL6pTSe<cjQ70--
TRIGGER.NSEVERITY=1
--.Yup38>&@AfooQ55*`nHefFI8c"GL6pTSe<cjQ70--
EVENT.DATE=2020.04.24
--.Yup38>&@AfooQ55*`nHefFI8c"GL6pTSe<cjQ70--
EVENT.TIME=15:01:22
--.Yup38>&@AfooQ55*`nHefFI8c"GL6pTSe<cjQ70--
EVENT.RECOVERY.DATE={EVENT.RECOVERY.DATE}
--.Yup38>&@AfooQ55*`nHefFI8c"GL6pTSe<cjQ70--
EVENT.RECOVERY.TIME={EVENT.RECOVERY.TIME}
"""


def test_messages_without_trailing_separator_should_be_rejected():
    with pytest.raises(modio_webhook.SeparatorMissing):
        modio_webhook.json_payload_from_alert_message(
            MISSING_LAST_SEPARATOR_MESSAGE
        )


INVALID_LINE_MESSAGE = """\
[webhook-event-values]
SEPARATOR=--.Yup38>&@AfooQ55*`nHefFI8c"GL6pTSe<cjQ70--
>>> invalid format line here <<<
--.Yup38>&@AfooQ55*`nHefFI8c"GL6pTSe<cjQ70--
EVENT.RECOVERY.ID={EVENT.RECOVERY.ID}
--.Yup38>&@AfooQ55*`nHefFI8c"GL6pTSe<cjQ70--
HOST.HOST=40bd32e308dc
--.Yup38>&@AfooQ55*`nHefFI8c"GL6pTSe<cjQ70--
ITEM.KEY=mytemp.status
--.Yup38>&@AfooQ55*`nHefFI8c"GL6pTSe<cjQ70--
TRIGGER.STATUS=PROBLEM
--.Yup38>&@AfooQ55*`nHefFI8c"GL6pTSe<cjQ70--
HOST.DESCRIPTION=Nilis Skrivbord
--.Yup38>&@AfooQ55*`nHefFI8c"GL6pTSe<cjQ70--
TRIGGER.ID=14389
--.Yup38>&@AfooQ55*`nHefFI8c"GL6pTSe<cjQ70--
TRIGGER.SEVERITY=Information
--.Yup38>&@AfooQ55*`nHefFI8c"GL6pTSe<cjQ70--
TRIGGER.NSEVERITY=1
--.Yup38>&@AfooQ55*`nHefFI8c"GL6pTSe<cjQ70--
EVENT.DATE=2020.04.24
--.Yup38>&@AfooQ55*`nHefFI8c"GL6pTSe<cjQ70--
EVENT.TIME=15:01:22
--.Yup38>&@AfooQ55*`nHefFI8c"GL6pTSe<cjQ70--
EVENT.RECOVERY.DATE={EVENT.RECOVERY.DATE}
--.Yup38>&@AfooQ55*`nHefFI8c"GL6pTSe<cjQ70--
EVENT.RECOVERY.TIME={EVENT.RECOVERY.TIME}
--.Yup38>&@AfooQ55*`nHefFI8c"GL6pTSe<cjQ70--
TRIGGER.NAME=Test
--.Yup38>&@AfooQ55*`nHefFI8c"GL6pTSe<cjQ70--"""


def test_messages_with_invalid_line_should_be_rejected():
    with pytest.raises(modio_webhook.ValueFormatMismatch):
        modio_webhook.json_payload_from_alert_message(INVALID_LINE_MESSAGE)


def test_messages_without_the_proper_format_should_be_rejected():
    with pytest.raises(modio_webhook.MessageFormatMismatch):
        modio_webhook.json_payload_from_alert_message("Some message")


SHORT_SEPARATOR_MESSAGE = """\
[webhook-event-values]
SEPARATOR=-
EVENT.ID=1177218
-
EVENT.RECOVERY.ID={EVENT.RECOVERY.ID}
-
HOST.HOST=40bd32e308dc
-
ITEM.KEY=mytemp.status
-
TRIGGER.STATUS=PROBLEM
-
HOST.DESCRIPTION=Nilis Skrivbord
-
TRIGGER.ID=14389
-
TRIGGER.SEVERITY=Information
-
TRIGGER.NSEVERITY=1
-
EVENT.DATE=2020.04.24
-
EVENT.TIME=15:01:22
-
EVENT.RECOVERY.DATE={EVENT.RECOVERY.DATE}
-
EVENT.RECOVERY.TIME={EVENT.RECOVERY.TIME}
-
TRIGGER.NAME=Test
-"""


def test_messages_with_too_short_separators_should_be_rejected():
    with pytest.raises(modio_webhook.MessageFormatMismatch):
        modio_webhook.json_payload_from_alert_message(SHORT_SEPARATOR_MESSAGE)
