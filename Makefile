
SUBDIRS = redis zabbix-server zabbix-web


.PHONY: all clean $(SUBDIRS)

all clean: $(SUBDIRS)

clean: TARGETS = clean

$(SUBDIRS):
	$(MAKE) -C $@ $(or $(TARGETS),build publish)
