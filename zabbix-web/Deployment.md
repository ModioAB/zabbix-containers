Port PHP-fpm:
    9000

Port nginx:
    8080

session dir:
    /tmp

Socket dir:
    /run/php/fpm.sock

Liveness check:
    /robots.txt

Ready Check:
    /-/ping
    /-/zabbix.php


PHP-Fpm healthcheck:
    liveness:
        /usr/local/bin/php-fpm-healthcheck
    readiness:
        # Not ready if we have a queue of 10 waiting requests
        /usr/local/bin/php-fpm-healthcheck --listen-queue=10
