#!/usr/bin/env python3

"""Alert script for making HTTPS webhook requests for Zabbix actions.

The media type and actions for this script are created by
submit.zabbix.Zabbix through zabbix_setup.CustomerZabbixSetup in the
submit server.

"""

try:
    from http.client import HTTPSConnection
except ImportError:
    from httplib import HTTPSConnection

import calendar
import json
import re
import ssl
import sys
import time

try:
    from urllib.parse import urlparse, urlunparse
except ImportError:
    from urlparse import urlparse, urlunparse


class MessageFormatMismatch(ValueError):
    """The overall format of the message does not match.

    This might be because it is a general action message sent through
    all media types.

    """


class SeparatorMissing(ValueError):
    """One of the message-specific separators are missing.

    The message is also expected to end with a separator.

    """


class RepeatedValueError(ValueError):
    """A value was specified more than once in the message.

    This is disallowed to make it more difficult to inject values into
    the message from user-modifiable strings.

    """


class ValueFormatMismatch(ValueError):
    """One of the values has an incorrect format.

    Values currently aren't allowed to contain line breaks.

    """


class ValueMissing(ValueError):
    """One of the required values isn't included in the message."""


REQUIRED_FIELDS = {
    "EVENT.DATE",
    "EVENT.ID",
    "EVENT.RECOVERY.DATE",
    "EVENT.RECOVERY.ID",
    "EVENT.RECOVERY.TIME",
    "EVENT.TIME",
    "HOST.DESCRIPTION",
    "HOST.HOST",
    "ITEM.KEY",
    "TRIGGER.ID",
    "TRIGGER.NAME",
    "TRIGGER.NSEVERITY",
    "TRIGGER.SEVERITY",
    "TRIGGER.STATUS",
}

# Zabbix tends to use DOS line breaks. I don't know if that's a
# guarantee, or if that just happens for multi-line values entered
# through the web UI.
LINE_BREAK = r"\r?\n"

# The format uses a separator between values that varies between
# action messages, and that isn't publicly known. This is to make it
# more difficult to exploit the absence of proper quoted Zabbix macro
# expansion in the action messages. It could be used to change related
# data in webhook messages by altering the user-modifiable strings,
# even though this is prevented by only allowing values to be
# specified once in the message.
MESSAGE = re.compile(
    r"^\[webhook-event-values\]"
    + LINE_BREAK
    + r"SEPARATOR=(\S{16,}?)"
    + LINE_BREAK
    + "(.*)$",
    flags=re.DOTALL,
)

KEY_AND_VALUE = re.compile(r"^((?:[A-Z]+)(?:\.[A-Z]+){1,2})=(.*)$")

# This is required to follow the exact convention used by the Modio
# API.
STATUS_MAPPING = {"PROBLEM": "problem", "OK": "OK"}


def json_payload_from_alert_message(message):
    match = MESSAGE.match(message)
    if not match:
        raise MessageFormatMismatch("Action not intended for this media type")

    separator, rest = match.group(1, 2)
    lines = re.split(LINE_BREAK, rest)

    values = {}
    current_key = None
    current_value = None
    for n, line in enumerate(lines):
        if n % 2 == 1:
            if line != separator:
                raise SeparatorMissing("Separator missing")
            else:
                continue
        if not line:
            continue
        match = KEY_AND_VALUE.match(line)
        if match:
            if current_key is not None:
                values[current_key] = current_value
            key, value = match.group(1, 2)
            if key in values:
                # This check is relevant for security reasons.
                raise RepeatedValueError("Value specified more than once")
            values[key] = value
        else:
            raise ValueFormatMismatch("Invalid format")

    if not values.keys() >= REQUIRED_FIELDS:
        raise ValueMissing("Required values are missing")

    status = STATUS_MAPPING.get(values["TRIGGER.STATUS"], "unknown")

    severity = (values["TRIGGER.NSEVERITY"], values["TRIGGER.SEVERITY"])

    if values["EVENT.RECOVERY.ID"] == "{EVENT.RECOVERY.ID}":
        # If this is not a recovery message, the macro does not expand.
        event_id = values["EVENT.ID"]
        event_date = values["EVENT.DATE"]
        event_time = values["EVENT.TIME"]
    else:
        event_id = values["EVENT.RECOVERY.ID"]
        event_date = values["EVENT.RECOVERY.DATE"]
        event_time = values["EVENT.RECOVERY.TIME"]

    event_time_string = "{0} {1}".format(event_date, event_time)
    event_time_tuple = time.strptime(event_time_string, "%Y.%m.%d %H:%M:%S")
    event_time = calendar.timegm(event_time_tuple)

    # The format is the same as the Modio API format for triggers and
    # events, minus the fields that cannot be provided through an
    # action message.
    payload = {
        "trigger": {
            "id": values["TRIGGER.ID"],
            "description": values["TRIGGER.NAME"],
            # "hosts": ...,
            "lastchange": event_time,
            "value": status,
            "severity": severity,
        },
        "event": {
            "eventid": event_id,
            "host": values["HOST.HOST"],
            "key": values["ITEM.KEY"],
            "value": status,
            "clock": event_time,
            "description": values["TRIGGER.NAME"],
            "severity": severity,
            # "acknowledgements": ...,
        },
    }

    return payload


def modio_post(sendto, message):
    url = urlparse(sendto)
    assert url.scheme == "https"

    tls_context = ssl.create_default_context()
    tls_context.load_cert_chain(
        certfile="/webhook/secret/tls.crt", keyfile="/webhook/secret/tls.key"
    )
    conn = HTTPSConnection(url.hostname, url.port, context=tls_context)

    path = urlunparse(("", "") + url[2:5] + ("",))
    payload = json_payload_from_alert_message(message)
    body = json.dumps(payload)
    headers = {}

    conn.request("POST", path, body, headers)

    response = conn.getresponse()
    response.read()

    if (response.status // 100) != 2:
        raise RuntimeError("{0} {1}".format(response.status, response.reason))


if __name__ == "__main__":
    if len(sys.argv) != 3:
        usage = "Usage: {0} SENDTO MESSAGE\n".format(sys.argv[0])
        sys.stderr.write(usage)
        sys.exit(1)

    modio_post(*sys.argv[1:])
